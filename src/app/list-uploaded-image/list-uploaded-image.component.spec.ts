import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUploadedImageComponent } from './list-uploaded-image.component';

describe('ListUploadedImageComponent', () => {
  let component: ListUploadedImageComponent;
  let fixture: ComponentFixture<ListUploadedImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUploadedImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUploadedImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
