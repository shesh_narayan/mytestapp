import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-test-inner-html',
  templateUrl: './test-inner-html.component.html',
  styleUrls: ['./test-inner-html.component.css']
})
export class TestInnerHtmlComponent implements OnInit {
  public invoiceForm: FormGroup;
  constructor(private _fb: FormBuilder) { }

  ngOnInit() {
    this.invoiceForm = this._fb.group({
      itemRows: this._fb.array([this.initItemRows()])
    });
  }

  initItemRows() {
    return this._fb.group({
        days: [''],
        from: [''],
        to: ['']
    });
}

addNewRow() {
  const control = <FormArray>this.invoiceForm.controls['itemRows'];
  control.push(this.initItemRows());
}

deleteRow(index: number) {
  const control = <FormArray>this.invoiceForm.controls['itemRows'];
  control.removeAt(index);
}

}
