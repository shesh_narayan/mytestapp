import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestInnerHtmlComponent } from './test-inner-html.component';

describe('TestInnerHtmlComponent', () => {
  let component: TestInnerHtmlComponent;
  let fixture: ComponentFixture<TestInnerHtmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestInnerHtmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestInnerHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
