// npm install @agm/core
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-google-map-test',
  templateUrl: './google-map-test.component.html',
  styleUrls: ['./google-map-test.component.css']
})
export class GoogleMapTestComponent implements OnInit {

  lat: number = 39.8282;
  lng: number = -98.5795;
  zoom:number = 8;
  label:string = "My Test Address";
  title:string = "My Test Address";
  constructor() { }

  ngOnInit() {
  }


}
