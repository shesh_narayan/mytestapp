// npm install ngx-uploader
import { Component, EventEmitter, OnInit } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;

  constructor() {
    this.options = { concurrency: 1, allowedContentTypes: ['image/png', 'image/jpeg', 'image/gif'] };
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  }

  ngOnInit() {
  }

  // onUploadOutput(output: UploadOutput): void {
  //   if (output.type === 'allAddedToQueue') {
  //     const event: UploadInput = {
  //       type: 'uploadAll',
  //       url: 'http://127.0.0.1/upload.php',
  //       method: 'POST',
  //       data: { foo: 'bar' }
  //     };

  //     this.uploadInput.emit(event);
  //   } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') {
  //     this.files.push(output.file);
  //   } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
  //     const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
  //     this.files[index] = output.file;
  //   } else if (output.type === 'removed') {
  //     this.files = this.files.filter((file: UploadFile) => file !== output.file);
  //   } else if (output.type === 'dragOver') {
  //     this.dragOver = true;
  //   } else if (output.type === 'dragOut') {
  //     this.dragOver = false;
  //   } else if (output.type === 'drop') {
  //     this.dragOver = false;
  //   } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
  //     console.log(output);
  //   }
  //   this.files = this.files.filter(file => file.progress.status !== UploadStatus.Done);
  // }

  startUpload(): void {
    const event: UploadInput = {
        type: 'uploadAll',
        url: 'http://127.0.0.1/upload.php',
        method: 'POST',
        data: { foo: 'bar' }
    };
    this.uploadInput.emit(event);
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }                                   

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }
}
