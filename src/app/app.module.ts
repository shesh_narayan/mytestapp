import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgUploaderModule } from 'ngx-uploader';
import { AgmCoreModule } from '@agm/core';


import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { ListUploadedImageComponent } from './list-uploaded-image/list-uploaded-image.component';
import { TestInnerHtmlComponent } from './test-inner-html/test-inner-html.component';
import { GoogleMapTestComponent } from './google-map-test/google-map-test.component';

const appRoutes: Routes = [
  { path: 'imageupload', component: ImageUploadComponent },
  { path: 'innerhtml', component: TestInnerHtmlComponent },
  { path: 'gmt', component: GoogleMapTestComponent },
  { path: 'listimages', component: ListUploadedImageComponent, data: { title: 'Images List' } },
  { path: '', redirectTo: '/listimages', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ImageUploadComponent,
    ListUploadedImageComponent,
    TestInnerHtmlComponent,
    GoogleMapTestComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    NgUploaderModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB-Qrke69q7z0Ll8RhG6BydMgB_kQplx-k'
    }),
    NgbModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
